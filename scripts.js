// Get toggles stored and ready for event listener        
const togglePanelElements = document.querySelectorAll('[data-js="toggle"]')

Array.from(togglePanelElements).forEach(toggle => toggle.addEventListener('click', function(){
    
    let panelToToggle = this.getAttribute('data-js-panel')
    let threeColsContainer = document.getElementsByClassName('threeCols')
    
    threeColsContainer[0].classList.toggle(panelToToggle)
}))